package id.cloudify.tool.hook;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

@SpringBootApplication
@RestController
public class Application {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	SimpleDateFormat filePattern = new SimpleDateFormat("yyyyMMddHHmmSSS");
	
	@RequestMapping(value="/", method=RequestMethod.POST)
	public void hook(@RequestBody Map data) {
		
		File json = new File(System.getenv("WORKDIR"), filePattern.format(new Date()).concat(".json"));
		logger.info(json.getName() + ":" + data.toString());
		try {
			FileUtils.writeStringToFile(json, new Gson().toJson(data));
		} catch (IOException e) {
			logger.error(json.getName().concat(" fail to write!"));
		}
		
	}
	
}

